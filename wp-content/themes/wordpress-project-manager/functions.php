<?php
/** Start the engine */
require_once( get_template_directory() . '/lib/init.php' );

/** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', 'WordPress Project Management' );
define( 'CHILD_THEME_URL', 'http://krtnb.ch' );


if( !is_user_logged_in() && $pagenow != 'wp-login.php' ) {
	wp_redirect( '/wp-login.php', 302 ); exit;
}

// Custom Meta Boxes
require_once 'lib/cmb/custom-meta-boxes.php';
require_once 'lib/cmb/settings.php';

// Load Sidebars
require 'lib/sidebar-defs.php';

// Load Custom Post Types + Custom Taxonomies
require 'lib/cpt-tax-defs.php';

// Load Posts 2 Posts
require 'lib/p2p-defs.php';

// Load Custom Comment Form (WYSIWYG)
require 'lib/custom-comment-form.php';


function projects_menu(){

	echo '<div id="nav"><div class="wrap"><ul class="genesis-nav-menu menu-primary">';

	$queried_object = get_queried_object();

	// var_dump($queried_object);

	if( $queried_object->post_type == 'projects' && $queried_object->post_parent == 0 ){

		echo '<li><a href="' . get_permalink() . '">' . $queried_object->post_title . '</a></li>';
	}
	elseif( $queried_object->post_parent != 0 ){

		echo '<li><a href="' . get_permalink( $queried_object->post_parent ) . '">' . get_the_title( $queried_object->post_parent ) . '</a></li>';
	}
	else{

		// Find connected projects
		$connected = new WP_Query(
			array(
				'post_type'		=> 'projects',
				'connected_type' => 'projects_to_tickets',
				'connected_items' => get_queried_object(),
				'nopaging' => true,
			)
		);

		// Display connected project
		if ( $connected->have_posts() ) :
			while ( $connected->have_posts() ) : $connected->the_post();
				?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php
			endwhile;

			// Thsi link is referenced below. (A)
			echo '<li><a href="' . get_permalink() . 'tickets/">Tickets</a></li>';

			// Prevent weirdness
			wp_reset_postdata();

		endif;
	}


	if( $queried_object->post_parent == 0 && $queried_object->post_type == 'tickets' ){

		// Don't show, link is shown above. (A)
	}
	elseif( $queried_object->post_parent == 0 && $queried_object->post_type == 'projects' ){

		echo '<li><a href="' . get_permalink() . 'tickets/">Tickets</a></li>';
	}
	else{
		echo '<li><a href="' . get_permalink( $queried_object->post_parent ) . 'tickets/">Tickets</a></li>';
	}
	echo '</ul></div></div>';
}



