<?php
/**
 * This file handles the single lay-out for projects
 *
 * @category WordPress Project Manager
 * @package  Templates
 * @author   Daan Kortenbach
 */

$queried_object = get_queried_object();

add_action( 'genesis_after_header', 'projects_menu' );


remove_action( 'genesis_loop', 'genesis_do_loop' );
remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );

remove_all_actions( 'genesis_after_post_title' );
remove_all_actions( 'genesis_before_post_content' );
remove_all_actions( 'genesis_after_post_content' );

add_action( 'genesis_loop', 'check_allowed' );
/**
 * check_allowed function.
 * 
 * @access public
 * @return void
 */
function check_allowed(){

	global $post, $current_user, $queried_object;

	$connected_posts = get_posts( array(
		'connected_type' => 'multiple_project_authors',
		'connected_items' => $current_user->ID,
		'suppress_filters' => false,
		'nopaging' => true
	) );

	$connected_posts_ids = array( 0 );
	foreach( $connected_posts as $key => $value ) {
		$connected_posts_ids[] = $connected_posts[$key]->ID;
	}


	if( in_array( get_the_ID(), $connected_posts_ids ) or current_user_can( 'manage_options' ) ){

		genesis_standard_loop();

		if( $queried_object->post_name == 'tickets' ){
			add_action( 'genesis_sidebar', 'before_sidebar', 1 );
		}
		add_action( 'genesis_sidebar', 'wppm_sidebar' );

	}
	else{
		echo 'You do not have access to this page.';
	}
}



if( $queried_object->post_name == 'tickets' ){
	remove_all_actions( 'genesis_post_content' );
	add_action( 'genesis_post_content', 'wppm_ticket_list' );
}
/**
 * wppm_ticket_list function.
 * 
 * @access public
 * @return void
 */
function wppm_ticket_list(){

	global $queried_object;

	$post = get_post( $queried_object->post_parent );

	// global $current_user;


	// Find connected posts
	$connected = new WP_Query( array(
	  'connected_type' => 'projects_to_tickets',
	  'connected_items' => $post,
	  'nopaging' => true,
	) );

	// Display connected posts
	if ( $connected->have_posts() ) :
	?>
	<ul>
	<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endwhile; ?>
	</ul>

	<?php 
	// Prevent weirdness
	wp_reset_postdata();

	endif;
}



if( $queried_object->post_name == 'new-ticket' ){
	remove_all_actions( 'genesis_post_content' );
	add_action( 'genesis_post_content', 'wppm_new_ticket' );
}
/**
 * wppm_ticket_list function.
 * 
 * @access public
 * @return void
 */
function wppm_new_ticket(){
	?>
	<form action="" method="post" name="addnewticket">
		<div class="form_field">
			<label for="tickettitle">Title:</label>
			<input type="text" name="tickettitle" id="tickettitle" style="width: 99%">
		</div>
		<div class="form_field">
			<label for="ticketcontent">Description:</label>
			<textarea name="ticketcontent" id="ticketcontent" style="width: 99%"></textarea>
		</div>
		<div class="form_field">
			<input type="submit">
		</div>
	</form>
	<?php
}


/**
 * wppm_sidebar function.
 * 
 * @access public
 * @return void
 */
function wppm_sidebar() {
	dynamic_sidebar( 'sidebar_single_project' );
}



/**
 * before_sidebar function.
 * 
 * @access public
 * @return void
 */
function before_sidebar(){
	global $queried_object;
	?>
	<div id="add_ticket_widget" class="widget widget_add_ticket">
		<div class="widget-wrap">
			<h4 class="widgettitle"><?php _e( 'Add New Ticket', 'wppm' ); ?></h4>
			<div class="textwidget">
				<a href="<?php echo get_permalink( $queried_object->post_parent ) ?>new-ticket/">New Ticket</a>
			</div>
		</div>
	</div>
	<?php
}



genesis();