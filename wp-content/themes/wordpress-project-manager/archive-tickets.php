<?php
/*
Template Name: Ticket Archives
*/

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'wppm_custom_loop' );
/**
 * Genesis custom loop
 */
function wppm_custom_loop() {

	global $post, $current_user;

	$connected_posts = get_posts( array(
		'connected_type' => 'multiple_ticket_authors',
		'connected_items' => $current_user->ID,
		'suppress_filters' => false,
		'nopaging' => true
	) );

	$connected_posts_ids = array( 0 );
	foreach( $connected_posts as $key => $value ) {
		$connected_posts_ids[] = $connected_posts[$key]->ID;
	}


	$args = array(
		'post_type'      => 'tickets',
		'posts_per_page' => 10,
		'post__in'		 => $connected_posts_ids,
		'post_status'    => 'publish',
		'paged'          => get_query_var( 'paged' )
	);

	if( current_user_can( 'manage_options' ) ){
		unset( $args['post__in'] );
	}

	/*
	Overwrite $wp_query with our new query.
	The only reason we're doing this is so the pagination functions work,
	since they use $wp_query. If pagination wasn't an issue,
	use: https://gist.github.com/3218106
	*/
	global $wp_query;
	$wp_query = new WP_Query( $args );

	if ( have_posts() ) :
		echo '<div class="entry-content"><ul>';
		while ( have_posts() ) : the_post();

			echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';

		endwhile;
		echo '</ul></div>';
		do_action( 'genesis_after_endwhile' );
	endif;

	wp_reset_query();
}



genesis();