<?php
/*
Template Name: Project Archives
*/

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'wppm_custom_loop' );
/**
 * Genesis custom loop
 */
function wppm_custom_loop() {

	global $current_user, $post, $wp_query;

	$connected_posts = get_posts( array(
		'connected_type' => 'multiple_project_authors',
		'connected_items' => $current_user->ID,
		'suppress_filters' => false,
		'nopaging' => true
	) );

	$connected_posts_ids = array( 0 );
	foreach( $connected_posts as $key => $value ) {
		$connected_posts_ids[] = $connected_posts[$key]->ID;
	}


	$args = array(
		'post_type'      => 'projects',
		'posts_per_page' => 10,
		'post_parent'	 => 0,
		'post__in'		 => $connected_posts_ids,
		'post_status'    => 'publish',
		'paged'          => get_query_var( 'paged' )
	);

	if( current_user_can( 'manage_options' ) ){
		unset( $args['post__in'] );
	}



	$wp_query = new WP_Query( $args );

	if ( have_posts() ) :
		echo '<div class="entry-content"><ul>';
		while ( have_posts() ) : the_post();

			echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';

		endwhile;
		echo '</ul></div>';
		do_action( 'genesis_after_endwhile' );
	endif;

	wp_reset_query();
}


add_action( 'genesis_sidebar', 'wppm_sidebar' );
/**
 * wppm_sidebar function.
 *
 * @access public
 * @return void
 */
function wppm_sidebar() {
	dynamic_sidebar( 'sidebar_projects' );
}


add_action( 'genesis_sidebar', 'before_sidebar', 1 );
/**
 * before_sidebar function.
 *
 * @access public
 * @return void
 */
function before_sidebar(){
	?>
	<div id="add_ticket_widget" class="widget widget_add_ticket">
		<div class="widget-wrap">
			<h4 class="widgettitle"><?php _e( 'Favorite Projects', 'wppm' ); ?></h4>
			<div class="textwidget">
				[list favorite projects]
			</div>
		</div>
	</div>
	<?php
}


genesis();