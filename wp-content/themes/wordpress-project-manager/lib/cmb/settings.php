<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category BeFrank
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/humanmade/Custom-Meta-Boxes/
 */
function wppm_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_wppm_';

	$fields = array(

		array( 'id' => $prefix . 'hours_communicated',  'name' => 'Hours communicated to the client', 'type' => 'text', 'cols' => 3 ),
		array( 'id' => $prefix . 'hours_internaly',  'name' => 'Internal agreed hours', 'type' => 'text', 'cols' => 3 ),
		array( 'id' => $prefix . 'ticket_discussion',  'name' => 'Ticket or Discussion name', 'type' => 'text', 'cols' => 3 ),
		array( 'id' => $prefix . 'related_ticket_URL',  'name' => 'Ticket name or number URL', 'type' => 'text', 'cols' => 3 ),
		array( 'id' => $prefix . 'extra_information',  'name' => 'Extra information', 'type' => 'wysiwyg', 'cols' => 12 ),

		);

		$meta_boxes[] = array(
				'title'		=> 'Ticket Metadata',
				'pages'		=> 'tickets',
				'priority'	=> 'high',
				'fields'	=> $fields
		);


	return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'wppm_metaboxes' );