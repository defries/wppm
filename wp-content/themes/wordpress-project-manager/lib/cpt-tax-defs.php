<?php
add_action( 'init', 'register_cpt_projects' );
/**
 * register_cpt_projects function.
 *
 * @access public
 * @return void
 */
function register_cpt_projects() {

	$labels = array(
		'name' 					=> _x( 'Projects', 'post type general name', 'wppm' ),
		'singular_name' 		=> _x( 'Project', 'post type singular name', 'wppm' ),
		'add_new' 				=> _x( 'Add New', 'project', 'wppm' ),
		'add_new_item' 			=> __( 'Add New Project', 'wppm' ),
		'edit_item' 			=> __( 'Edit Project', 'wppm' ),
		'new_item' 				=> __( 'New Project', 'wppm' ),
		'all_items' 			=> __( 'All Projects', 'wppm' ),
		'view_item' 			=> __( 'View Project', 'wppm' ),
		'search_items' 			=> __( 'Search Projects', 'wppm' ),
		'not_found' 			=> __( 'No projects found', 'wppm' ),
		'not_found_in_trash' 	=> __( 'No projects found in Trash', 'wppm' ),
		'parent_item_colon' 	=> '',
		'menu_name' 			=> __( 'Projects', 'wppm' )
	);

	$args = array(
		'labels' 				=> $labels,
		'hierarchical' 			=> true,
		'description' 			=> 'Custom Post Type for Projects',
		'supports' 				=> array(
			'title',
			'slug',
			'editor',
			'custom-fields',
			'page-attributes'
			),
		'public' 				=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'show_in_nav_menus' 	=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'has_archive' 			=> false,
		'query_var' 			=> true,
		'can_export' 			=> true,
		'rewrite' 				=> array(
			'slug' 				=> 'project' ),
		'capability_type' 		=> 'post'
	);

	register_post_type( 'projects', $args );
}


add_action( 'init', 'register_cpt_tickets' );
/**
 * register_cpt_tickets function.
 *
 * @access public
 * @return void
 */
function register_cpt_tickets() {

	$labels = array(
		'name' 					=> _x( 'Tickets', 'post type general name', 'wppm' ),
		'singular_name' 		=> _x( 'Ticket', 'post type singular name', 'wppm' ),
		'add_new' 				=> _x( 'Add New', 'ticket', 'wppm' ),
		'add_new_item' 			=> __( 'Add New Ticket', 'wppm' ),
		'edit_item' 			=> __( 'Edit Ticket', 'wppm' ),
		'new_item' 				=> __( 'New Ticket', 'wppm' ),
		'all_items' 			=> __( 'All Tickets', 'wppm' ),
		'view_item' 			=> __( 'View Ticket', 'wppm' ),
		'search_items' 			=> __( 'Search Tickets', 'wppm' ),
		'not_found' 			=> __( 'No tickets found', 'wppm' ),
		'not_found_in_trash' 	=> __( 'No tickets found in Trash', 'wppm' ),
		'parent_item_colon' 	=> '',
		'menu_name' 			=> __( 'Tickets', 'wppm' )
	);

	$args = array(
		'labels' 				=> $labels,
		'hierarchical' 			=> false,
		'description' 			=> 'Custom Post Type for Tickets',
		'supports' 				=> array(
			'title',
			'slug',
			'editor',
			'custom-fields',
			'author',
			'comments'
			),
		'public' 				=> true,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'show_in_nav_menus' 	=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'has_archive' 			=> true,
		'query_var' 			=> true,
		'can_export' 			=> true,
		'rewrite' 				=> array(
			'slug' 				=> 'ticket' ),
		'capability_type' 		=> 'post'
	);

	register_post_type( 'tickets', $args );
}