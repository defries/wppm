<?php

/** Register widget areas */
genesis_register_sidebar(
	array(
    	'id'            => 'sidebar_projects',
    	'name'          => __( 'Projects', 'wppm' ),
    	'description'   => __( 'This is the Projects sidebar', 'wppm' ),
	)
);

genesis_register_sidebar(
	array(
    	'id'            => 'sidebar_single_project',
    	'name'          => __( 'Single Project', 'wppm' ),
    	'description'   => __( 'This is the Single Project sidebar', 'wppm' ),
	)
);

genesis_register_sidebar(
	array(
    	'id'            => 'sidebar_tickets',
    	'name'          => __( 'Tickets', 'wppm' ),
    	'description'   => __( 'This is the Tickets sidebar', 'wppm' ),
	)
);

genesis_register_sidebar(
	array(
    	'id'            => 'sidebar_single_ticket',
    	'name'          => __( 'Single Ticket', 'wppm' ),
    	'description'   => __( 'This is the Single Tickets sidebar', 'wppm' ),
	)
);