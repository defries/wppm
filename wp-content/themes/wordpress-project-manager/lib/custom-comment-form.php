<?php
add_filter( 'comment_form_defaults', 'custom_comment_form_defaults' );
/**
 * custom_comment_form_defaults function.
 * 
 * @access public
 * @return void
 */
function custom_comment_form_defaults( $args ) {
	if ( is_user_logged_in() ) {
		$mce_plugins = 'inlinepopups, fullscreen, wordpress, wplink, wpdialogs';
	} else {
		$mce_plugins = 'fullscreen, wordpress';
	}
	ob_start();
	wp_editor( '', 'comment', array(
		'media_buttons' => true,
		'teeny' => true,
		'textarea_rows' => '7',
		'tinymce' => array( 'plugins' => $mce_plugins )
	) );
	$args['comment_field'] = ob_get_clean();
	return $args;
}

add_action( 'wp_enqueue_scripts', 'wppm_scripts' );
/**
 * wppm_scripts function.
 * 
 * @access public
 * @return void
 */
function wppm_scripts() {
	wp_enqueue_script('jquery');
}

add_filter( 'comment_reply_link', 'wppm_comment_reply_link' );
/**
 * wppm_comment_reply_link function.
 * 
 * @access public
 * @return void
 */
function wppm_comment_reply_link($link) {
	return str_replace( 'onclick=', 'data-onclick=', $link );
}

add_action( 'wp_head', 'wppm_wp_head' );
/**
 * wppm_wp_head function.
 * 
 * @access public
 * @return void
 */
function wppm_wp_head() {
?>
<script type="text/javascript">
	jQuery(function($){
		$('.comment-reply-link').click(function(e){
			e.preventDefault();
			var args = $(this).data('onclick');
			args = args.replace(/.*\(|\)/gi, '').replace(/\"|\s+/g, '');
			args = args.split(',');
			tinymce.EditorManager.execCommand('mceRemoveControl', true, 'comment');
			addComment.moveForm.apply( addComment, args );
			tinymce.EditorManager.execCommand('mceAddControl', true, 'comment');
		});
	});
</script>
<?php
}