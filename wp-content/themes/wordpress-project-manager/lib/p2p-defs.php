<?php

// include load.php
// include 'load.php';


add_action( 'p2p_init', 'wppm_connection_types' );
/**
 * wppm_connection_types function.
 * 
 * @access public
 * @return void
 */
function wppm_connection_types() {

	p2p_register_connection_type(
		array(
			'name'			=> 'projects_to_tickets',
			'from'			=> 'projects',
			'to'			=> 'tickets',
			'cardinality' 	=> 'one-to-many'
		)
	);

	p2p_register_connection_type(
		array(
			'name' => 'multiple_project_authors',
			'from' => 'projects',
			'to' => 'user'
		)
	);

	p2p_register_connection_type( 
		array(
			'name' => 'multiple_ticket_authors',
			'from' => 'tickets',
			'to' => 'user'
		)
	);
}

// add_action( 'wp_insert_post', 'wppm_add_users_to_tickets' );
/**
 * wppm_add_users_to_tickets function.
 * 
 * @access public
 * @return void
 */
function wppm_add_users_to_tickets(){

	// // Find connected projects
	// $connected = new WP_Query( 
	// 	array(
	// 		'post_type'		=> 'projects',
	// 		'connected_type' => 'tickets_to_projects',
	// 		'connected_items' => get_queried_object(),
	// 		'nopaging' => true,
	// 	)
	// );

	// // Display connected pages
	// if ( $connected->have_posts() ) :
	// 	while ( $connected->have_posts() ) : $connected->the_post();
	// 		##
	// 	endwhile; 

	// 	// Prevent weirdness
	// 	wp_reset_postdata();

	// endif;

	// $users = get_users(
	// 	array(
	// 		'connected_type' => 'multiple_project_authors',
	// 		'connected_items' => $post
	// 	)
	// );
}