<?php
/**
 * This file handles the single lay-out for tickets
 *
 * @category WordPress Project Manager
 * @package  Templates
 * @author   Daan Kortenbach
 */

add_action( 'genesis_after_header', 'projects_menu' );

remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'check_allowed' );

function check_allowed(){

	global $post, $current_user;

	$connected_posts = get_posts( array(
		'connected_type' => 'multiple_ticket_authors',
		'connected_items' => $current_user->ID,
		'suppress_filters' => false,
		'nopaging' => true
	) );

	$connected_posts_ids = array( 0 );
	foreach( $connected_posts as $key => $value ) {
		$connected_posts_ids[] = $connected_posts[$key]->ID;
	}


	if( in_array( get_the_ID(), $connected_posts_ids ) or current_user_can( 'manage_options' ) ){

		genesis_standard_loop();

		add_action( 'genesis_sidebar', 'wppm_sidebar' );
	}
	else{
		echo 'You do not have access to this project.';
	}
}



remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );

/**
 * wppm_sidebar function.
 *
 * @access public
 * @return void
 */
function wppm_sidebar() {
	dynamic_sidebar( 'sidebar_single_ticket' );
}

add_action( 'genesis_before_sidebar_widget_area', 'wppm_metadata', 99 );
/**
 * Echoing Ticket Meta Data
 * @return null
 */
function wppm_metadata() {

	echo '<div class="widget">';
	echo '<h4>Ticket information</h4><dl>';
		if ( $hours_communicated = get_post_meta( get_the_ID(), $key = '_wppm_hours_communicated', $single = true ) ) {
			echo '<dt>Hours Communicated:</dt> <dd>'. $hours_communicated . '</dd>';
		}
		if ( $hours_internaly = get_post_meta( get_the_ID(), $key = '_wppm_hours_internaly', $single = true ) ) {
			echo '<dt>Hours used internally:</dt> <dd>'. $hours_internaly . '</dd>';
		}

		$related_ticket_URL = get_post_meta( get_the_ID(), $key = '_wppm_related_ticket_URL', $single = true );

		if ( $ticket_discussion = get_post_meta( get_the_ID(), $key = '_wppm_hours_communicated', $single = true ) ) {
			echo '<dt>Related Ticket / Discussion:</dt> <dd><a href="'. $related_ticket_URL . '">'. $ticket_discussion . '</a>';
		}

	echo '</ul>';

	if ( $extra_information = get_post_meta( get_the_ID(), $key = '_wppm_extra_information', $single = true ) ) {
		echo '<p>'. $extra_information . '</p>';
	}

	echo '</div>';


}




genesis();