<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {

	include( dirname( __FILE__ ) . '/local-config.php' );
	define( 'WP_LOCAL_DEV', true );
}
else {
	define('DB_NAME', 'wppm');

	/** MySQL database username */
	define('DB_USER', 'wppm');

	/** MySQL database password */
	define('DB_PASSWORD', 'e5ff1e08256e2f42d2029a3c1a8d501a');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R2j@p2L!- SjWI4^evmi![|EIWSsbHbb8c*cbf6AoFq5-+9FS5K8|:p9=uFhF`fT');
define('SECURE_AUTH_KEY',  'eqIP15O%6D7|?}b#Y>|xfiX0wqjVhxY7n+67;6WP.Ca`:%F|^/yaR5d6?q.S*Vih');
define('LOGGED_IN_KEY',    'c=?l]+*vIo#vJ|HNevY&:]7 eM_:!gIs&{=oe{`m3yJP1</FM39rf#)>17FuQo|*');
define('NONCE_KEY',        '?|ubFG-${b:lU#|&3ZQ<vYL5k~UB.,iLB` *KkA~<{&Z/-*!H6,Lh;kd<t?UWqt+');
define('AUTH_SALT',        'G!3sQwL+ mTIJg5wv3crLN~wS7qBk ]-j gktjX*H~6/&c7|$,ea2)G}e1xYy6=2');
define('SECURE_AUTH_SALT', '/cBk^W-r[Nj&IrFm$l=.uiKGc<:+1-&MA0h|xR(#P62U~]c;T+<gC#Df0^@+p|o*');
define('LOGGED_IN_SALT',   'RXFq:bof^tqO=VNuf&8I$k+A!Ci(0z%WPqxZX*AczZR*_13@h4Fb/]:yg}%.:&ny');
define('NONCE_SALT',       'T$f+=8jW[A-HRA=.(JF]$59hq$CAnaosoaLRDpm/RlrcCt5-*N/3*?/~d*eKd0A}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
